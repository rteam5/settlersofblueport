﻿using System;
using SettlersOfBlueport.Data.Enums;
using SettlersOfBlueport.Data.Models;
using SettlersOfBlueport.Services.Services;
using NUnit.Framework;

namespace SettlersOfBlueport.Tests.Services
{
    [TestFixture]
    public class GameServiceTest
    {
        [Test]
        public void AddValidPlayerToGame()
        {
            var player = new Player("Test name", ColorEnum.Blue);

            //var game = new Game();

            var gameService = new GameService();


        }

        [Test]
        public void CannotAddInvalidPlayerToGame()
        {
            
        }

        //todo Test for Special characters in the future
        [Test]
        public void IsPlayerValid()
        {
            var player = new Player("Test Name", ColorEnum.Blue);
            var gameService = new GameService();
            var result = gameService.IsPlayerValid(player);

            Assert.True(result);
            Assert.IsTrue(!String.IsNullOrWhiteSpace(player.Name));
            Assert.IsTrue(player.Name.Length <= 20);
        }

        [Test]
        public void IsPlayerInvalidWithEmptyString()
        {
            var player = new Player("", ColorEnum.Blue);
            var game = new GameService();
            var result = game.IsPlayerValid(player);

            Assert.False(result);
            Assert.IsTrue(String.IsNullOrWhiteSpace(player.Name));
        }

        [Test]
        public void IsPlayerInvalidWithTooLongName()
        {
            var player = new Player("This is a really really really really long name", ColorEnum.Blue);
            var game = new GameService();
            var result = game.IsPlayerValid(player);

            Assert.False(result);
            Assert.IsTrue(player.Name.Length > 20);
        }


        [Test]
        public void GameHasValidNumberOfPlayers()
        {
            
        }

    }
}

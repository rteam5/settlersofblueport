﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SettlersOfBlueport.Data.Enums;

namespace SettlersOfBlueport.Data.Models
{
    public class Player
    {
        public string Name { get; private set; }
        public ColorEnum Color { get; private set; }

        public Player(string name, ColorEnum color)
        {
            Name = name;
            Color = color;
        }

    }
}
﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SettlersOfBlueport.Startup))]
namespace SettlersOfBlueport
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
